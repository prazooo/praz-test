/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.adtruth.zander.utility;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;

/**
 * ThreadLocal object mapper.
 */
public enum ZanderObjectMapper {
	INSTANCE;
	private ObjectMapper mapper = null;
private String x = "X";
private String y = "Y";
    private final ThreadLocal<ObjectMapper> objectMapperHolder = new ThreadLocal<ObjectMapper>() {
        @Override
        protected ObjectMapper initialValue() {
            final ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JodaModule());
            mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, true);
            return mapper;
        }
    };
    
    private ZanderObjectMapper() {
    	mapper = new ObjectMapper();
    	mapper.registerModule(new JodaModule());
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, true);
    }

    public ObjectMapper getThreadLocalObjectMapper() {
	System.out.println("3rd & 4th");
        return objectMapperHolder.get();
    }
    
    public ObjectMapper getObjectMapper() {
	System.out.println("1st");
        return mapper;
    }
}
