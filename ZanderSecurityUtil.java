/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.adtruth.zander.utility;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import com.adtruth.zander.exception.ZanderException;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Utility methods related to security.
 */
public enum ZanderSecurityUtil {
    // INSTANCE
    INSTANCE;
    
    // HMAC comment
    private static final String HMAC_SHA256 = "HmacSHA256";

    private final static Logger LOG = LoggerFactory.getLogger(ZanderSecurityUtil.class);
    
    /**
     * To get the HMAC hash code.
     * @param message - Message to be used in the hash.
     * @param secret - Key.
     * @return Hash value.
     */
    public String getHash(final String message, final String secret) {
        final Mac sha256HMAC;
        try {
            sha256HMAC = Mac.getInstance(HMAC_SHA256);
            final SecretKeySpec secretKey = new SecretKeySpec(secret.getBytes(), HMAC_SHA256);
            sha256HMAC.init(secretKey);
            final String hash = Base64.encodeBase64String(sha256HMAC.doFinal(message.getBytes()));
            return hash;
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            LOG.error("An unexpected error occurred while hashing.", e);
            throw new ZanderException("An unexpected error occurred while hashing.");
        }

    }
    
}
